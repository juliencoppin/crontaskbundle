<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:19
 */

namespace JulienCoppin\CronTaskBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CronTaskRepository extends EntityRepository
{
    /**
     * @return CronTask[]
     */
    public function findAllActive()
    {
        $qb = $this->createQueryBuilder('ct')
            ->where('ct.cronTaskActive = TRUE');

        return $qb->getQuery()->getResult();
    }
}