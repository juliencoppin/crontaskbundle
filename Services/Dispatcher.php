<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:31
 */

namespace JulienCoppin\CronTaskBundle\Services;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class Dispatcher
{
    /**
     * @var Container
     */
    private $container;

    /**
     * Dispatcher constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function dispatchCronTask(CronTask $cronTask, \DateTime $runTime)
    {
        if (!$this->isRunnable($cronTask, $runTime)) {
            return;
        }

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        if (!$this->container->has($cronTask->getCronTaskServiceName())) {
            $history = new CronTaskHistory();
            $history->setCronTask($cronTask);
            $history->setCronTaskHistoryResult(false);
            $history->setCronTaskHistoryExceptionMessage(sprintf("Unknow service name : %s", $cronTask->getCronTaskServiceName()));
            $em->persist($history);
            $em->flush();
        }

        try {
            $service = $this->container->get($cronTask->getCronTaskServiceName());
            if (!$service instanceof ICronTask) {

            }
        } catch (\Exception $e) {

        }
    }

    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @return bool
     */
    private function isRunnable(CronTask $cronTask, \DateTime $runTime)
    {
        if (strcmp($cronTask->getCronTaskTime()->format('H:i'), $runTime->format('H:i')) == 0) {
            if ($cronTask->getCronTaskDayOfMonth() !== null && $cronTask->getCronTaskDayOfWeek() !== null) {
                return false;
            }
            else if ($cronTask->getCronTaskDayOfMonth() !== null) {
                return ($cronTask->getCronTaskDayOfMonth() == (int)$runTime->format('d'));
            } else if ($cronTask->getCronTaskDayOfWeek() !== null) {
                return ($cronTask->getCronTaskDayOfWeek() == (int)$runTime->format('N'));
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * @param EntityManager $em
     * @param CronTask $cronTask
     * @param bool $result
     * @param null $message
     */
    private function generateHistory(EntityManager $em, CronTask $cronTask, $result = true, $message = null)
    {
        $history = new CronTaskHistory();
        $history->setCronTask($cronTask);
        $history->setCronTaskHistoryResult($result);
        $history->setCronTaskHistoryExceptionMessage($message);
        $em->persist($history);
        $em->flush();
    }
}