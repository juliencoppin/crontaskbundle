<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-11-16
 * Time: 16:17
 */

namespace JulienCoppin\CronTaskBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronTaskCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName("juliencoppin:cron:run")
            ->setDescription("Run the cron tasks");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $runTime = new \DateTime();

        $cronTasks = $em->getRepository('ImprimerieMainBundle:CronTask')->findAllActive();
        $dispatcher = $this->getContainer()->get('');

        foreach ($cronTasks as $cronTask) {
            $toRun = false;

        }
    }
}