<?php

namespace JulienCoppin\CronTaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CronTaskHistory
 *
 * @ORM\Table(name="CronTasksHistory")
 * @ORM\Entity
 */
class CronTaskHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskHistoryID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $cronTaskHistoryID;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CronTaskHistoryResult", type="boolean", nullable=false)
     */
    private $cronTaskHistoryResult;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskHistoryExceptionMessage", type="text", nullable=true)
     */
    private $cronTaskHistoryExceptionMessage;

    /**
     * @ORM\ManyToOne(targetEntity="Imprimerie\MainBundle\Entity\CronTask")
     * @ORM\JoinColumn(name="CronTaskID", referencedColumnName="CronTaskID", nullable=false)
     */
    private $cronTask;

    /**
     * Get cronTaskHistoryID
     *
     * @return integer
     */
    public function getCronTaskHistoryID()
    {
        return $this->cronTaskHistoryID;
    }

    /**
     * Set cronTaskHistoryResult
     *
     * @param boolean $cronTaskHistoryResult
     *
     * @return CronTaskHistory
     */
    public function setCronTaskHistoryResult($cronTaskHistoryResult)
    {
        $this->cronTaskHistoryResult = $cronTaskHistoryResult;

        return $this;
    }

    /**
     * Get cronTaskHistoryResult
     *
     * @return boolean
     */
    public function getCronTaskHistoryResult()
    {
        return $this->cronTaskHistoryResult;
    }

    /**
     * Set cronTaskHistoryExceptionMessage
     *
     * @param string $cronTaskHistoryExceptionMessage
     *
     * @return CronTaskHistory
     */
    public function setCronTaskHistoryExceptionMessage($cronTaskHistoryExceptionMessage)
    {
        $this->cronTaskHistoryExceptionMessage = $cronTaskHistoryExceptionMessage;

        return $this;
    }

    /**
     * Get cronTaskHistoryExceptionMessage
     *
     * @return string
     */
    public function getCronTaskHistoryExceptionMessage()
    {
        return $this->cronTaskHistoryExceptionMessage;
    }

    /**
     * Set cronTask
     *
     * @param \Imprimerie\MainBundle\Entity\CronTask $cronTask
     *
     * @return CronTaskHistory
     */
    public function setCronTask(\Imprimerie\MainBundle\Entity\CronTask $cronTask)
    {
        $this->cronTask = $cronTask;

        return $this;
    }

    /**
     * Get cronTask
     *
     * @return \Imprimerie\MainBundle\Entity\CronTask
     */
    public function getCronTask()
    {
        return $this->cronTask;
    }
}
