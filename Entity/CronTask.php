<?php

namespace JulienCoppin\CronTaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CronTask
 *
 * @ORM\Table(name="CronTasks")
 * @ORM\Entity(repositoryClass="JulienCoppin\CronTaskBundle\Repository\CronTaskRepository")
 */
class CronTask
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $cronTaskID;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskName", type="string", nullable=false, length=255)
     */
    private $cronTaskName;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskServiceName", type="string", nullable=false, length=255)
     */
    private $cronTaskServiceName;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="CronTaskTime", type="time", nullable=false)
     */
    private $cronTaskTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskDayOfWeek", type="integer", nullable=true)
     */
    private $cronTaskDayOfWeek;

    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskDayOfMonth", type="integer", nullable=true)
     */
    private $cronTaskDayOfMonth;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CronTaskActive", type="boolean", nullable=false)
     */
    private $cronTaskActive;

    /**
     * Get cronTaskID
     *
     * @return integer
     */
    public function getCronTaskID()
    {
        return $this->cronTaskID;
    }

    /**
     * Set cronTaskName
     *
     * @param string $cronTaskName
     *
     * @return CronTask
     */
    public function setCronTaskName($cronTaskName)
    {
        $this->cronTaskName = $cronTaskName;

        return $this;
    }

    /**
     * Get cronTaskName
     *
     * @return string
     */
    public function getCronTaskName()
    {
        return $this->cronTaskName;
    }

    /**
     * Set cronTaskServiceName
     *
     * @param string $cronTaskServiceName
     *
     * @return CronTask
     */
    public function setCronTaskServiceName($cronTaskServiceName)
    {
        $this->cronTaskServiceName = $cronTaskServiceName;

        return $this;
    }

    /**
     * Get cronTaskServiceName
     *
     * @return string
     */
    public function getCronTaskServiceName()
    {
        return $this->cronTaskServiceName;
    }

    /**
     * Set cronTaskTime
     *
     * @param \DateTime $cronTaskTime
     *
     * @return CronTask
     */
    public function setCronTaskTime($cronTaskTime)
    {
        $this->cronTaskTime = $cronTaskTime;

        return $this;
    }

    /**
     * Get cronTaskTime
     *
     * @return \DateTime
     */
    public function getCronTaskTime()
    {
        return $this->cronTaskTime;
    }

    /**
     * Set cronTaskDayOfWeek
     *
     * @param integer $cronTaskDayOfWeek
     *
     * @return CronTask
     */
    public function setCronTaskDayOfWeek($cronTaskDayOfWeek)
    {
        $this->cronTaskDayOfWeek = $cronTaskDayOfWeek;

        return $this;
    }

    /**
     * Get cronTaskDayOfWeek
     *
     * @return integer
     */
    public function getCronTaskDayOfWeek()
    {
        return $this->cronTaskDayOfWeek;
    }

    /**
     * Set cronTaskDayOfMonth
     *
     * @param integer $cronTaskDayOfMonth
     *
     * @return CronTask
     */
    public function setCronTaskDayOfMonth($cronTaskDayOfMonth)
    {
        $this->cronTaskDayOfMonth = $cronTaskDayOfMonth;

        return $this;
    }

    /**
     * Get cronTaskDayOfMonth
     *
     * @return integer
     */
    public function getCronTaskDayOfMonth()
    {
        return $this->cronTaskDayOfMonth;
    }

    /**
     * Set cronTaskActive
     *
     * @param boolean $cronTaskActive
     *
     * @return CronTask
     */
    public function setCronTaskActive($cronTaskActive)
    {
        $this->cronTaskActive = $cronTaskActive;

        return $this;
    }

    /**
     * Get cronTaskActive
     *
     * @return boolean
     */
    public function getCronTaskActive()
    {
        return $this->cronTaskActive;
    }
}
